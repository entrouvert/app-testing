const assert = require('assert')

const { Given, When, Then, After, AfterAll, setDefaultTimeout} = require('@cucumber/cucumber');
const { Builder, By, Capabilities, Key } = require('selenium-webdriver');
const { expect } = require('chai');
setDefaultTimeout(5 * 1000);
require("chromedriver");

// driver setup
const capabilities = Capabilities.chrome();
capabilities.set('chromeOptions', { "w3c": false });
const driver = new Builder().withCapabilities(capabilities).build();
driver.manage().window().maximize()

Given("je suis sur la page d'accueil de Combo", async function () {
  await driver.get('https://combo.dev.publik.love');
});

Given('je visite la page de connexion', async function () {
  let element = await driver.findElement(By.className('login-link'));
  await element.click();
});


When('je saisis {string} dans le champ {string}', async function (text, field) {
  let element = await driver.findElement(By.name(field));
  await element.sendKeys(text)
});

When('je valide le formulaire', async function () {
  let currentField = await driver.switchTo().activeElement();
  await currentField.sendKeys(Key.RETURN)
});


Then('Je dois être connectée en tant que {string}', async function (expectedUsername) {
  let usernameLink = await driver.findElement(By.className('connected-user'));
  let username = await usernameLink.getText()
  expect(username).to.equal(expectedUsername)
});

When('je clique sur {string}', async function (text) {
  let element = await driver.findElement(By.xpath(`//*[contains(text(), "${text}")]`));
  await element.sendKeys(Key.RETURN);
});


Then('Je dois ne dois pas être connecté', async function () {
  await driver.findElement(By.className('login-link'));
});

Then('la page doit afficher {string}', async function (text) {
  let element = await driver.findElement(By.xpath(`//*[contains(text(), "${text}")]`));
});

AfterAll(async function(){
  await driver.quit();
});