Fonctionnalité: Authentik
  Contexte:
    Sachant que je suis sur la page d'accueil de Combo

  Scénario: Connexion valide
    Sachant que je visite la page de connexion
    Lorsque je saisis "admin@localhost" dans le champ "username"
    Et que je saisis "admin" dans le champ "password"
    Et que je valide le formulaire
    Alors Je dois être connectée en tant que "admin admin"
    
  Scénario: Déconnexion
    Lorsque je clique sur "Déconnexion"
    Alors Je dois ne dois pas être connecté
  
  Scénario: Authentification échouée
    Sachant que je visite la page de connexion
    Lorsque je saisis "admin@localhost" dans le champ "username"
    Et que je saisis "invalide" dans le champ "password"
    Et que je valide le formulaire
    Alors Je dois ne dois pas être connecté
    Et la page doit afficher "Courriel ou mot de passe incorrect"