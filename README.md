# Utilitaires de test pour les applications Publik

Bibliothèque permettant d'exécuter des spécifications fonctionnelles sous forme de test.

Pour utiliser :

```bash
npm install
npm test
```

Cela devrait lancer un navigateur et exécuter les étapes de test implémentées dans `features/*.feature`.
